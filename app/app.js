var app = angular.module('app',['ngRoute','ngResource','ui.bootstrap','ng-fusioncharts']);

app.config(function($routeProvider, $locationProvider)
{
   // remove o # da url
   $locationProvider.html5Mode(true);

   // configura as rotas, definindo rota, template e controller
   $routeProvider
   .when('/', {
      templateUrl : 'app/views/home.html',
      controller     : 'HomeCtrl',
   })
   .when('/editar/:id', {
      templateUrl : '../app/views/editar.html',
      controller  : 'EditarCtrl',
   })
   .when('/cadastrar', {
      templateUrl : 'app/views/cadastrar.html',
      controller  : 'CadastrarCtrl',
   })
   .when('/relatorios', {
      templateUrl : '../app/views/relatorios.html',
      controller  : 'RelatoriosCtrl',
   })
   .otherwise({ redirectTo: '/' });

});

app.factory('Pessoas', ['$resource', function($resource) {
   return $resource('https://test-frontend-neppo.herokuapp.com/pessoas/:id', null,
   {
      'query': {method:'GET', params:{id:'.json'}, isArray:true},
      'get': {method:'GET', params:{id:''}},
      'update': { method:'PUT'},
      'create': { method: "POST"},
      'remove': { method: "DELETE"}
   });
}]);

