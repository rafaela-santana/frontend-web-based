<?php

namespace controllers{

	class Pessoa{

		private $PDO;
 
		// Conecta ao banco de dados
		function __construct(){
			$this->PDO = new \PDO('mysql:host=localhost;dbname=pessoas', 'root', '');
			$this->PDO->setAttribute( \PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION );
		}

		// Insere uma nova pessoa no banco
		public function cadastrar(){

			global $app;

			$dados = json_decode($app->request->getBody(), true);
			$dados = (sizeof($dados)==0)? $_POST : $dados;
			$chaves = array_keys($dados); 

			$pessoas_sql = $this->PDO->prepare("
				INSERT INTO pessoas (".implode(',', $chaves).") 
				VALUES (:".implode(",:", $chaves).")
			");

			foreach ($dados as $chave => $valor) {
				$pessoas_sql ->bindValue(':'.$chave,$valor);
			}
			$pessoas_sql->execute();

			//Retorna o id inserido
			$app->render('default.php',["data"=>['id'=>$this->PDO->lastInsertId()]],200); 

		}

		// Pega pessoa pelo id
		public function pegar($id){

			global $app;

			$pessoa_sql = $this->PDO->prepare("
				SELECT * FROM pessoas WHERE id = :id
			");
			$pessoa_sql->bindValue(':id',$id);
			$pessoa_sql->execute();

			$pessoa = $pessoa_sql->fetch(\PDO::FETCH_ASSOC);

			$app->render('default.php',["data"=>$pessoa],200); 

		}

		// Edita pessoa no banco
		public function editar($id){
			global $app;
			$dados = json_decode($app->request->getBody(), true);
			$dados = (sizeof($dados)==0)? $_POST : $dados;
			$conjuntos = [];
			foreach ($dados as $chave => $valor) {
				$conjuntos[] = $chave." = :".$chave;
			}
 
			$pessoas_sql = $this->PDO->prepare("
				UPDATE 
					pessoas 
				SET ".implode(',', $conjuntos)." 
				WHERE id = :id
			");

			$pessoas_sql ->bindValue(':id',$id);
			foreach ($dados as $chave => $valor) {
				$pessoas_sql->bindValue(':'.$chave,$valor);
			}

			//Retorna status
			$app->render('default.php',["data"=>['status'=>$pessoas_sql->execute()==1]],200); 
		}
 	
 		// Exclui pessoa do banco
		public function excluir($id){
			global $app;

			$pessoas_sql = $this->PDO->prepare("
				DELETE FROM pessoas
				WHERE id = :id
			");
			
			$pessoas_sql ->bindValue(':id',$id);

			//Retorna status
			$app->render('default.php',["data"=>['status'=>$pessoas_sql->execute()==1]],200); 
		}

		// Lista pessoas
		public function listar(){

			global $app;

			$pessoas_sql = $this->PDO->prepare("
				SELECT * FROM pessoas
			");
			$pessoas_sql->execute();

			$pessoas = $pessoas_sql->fetchAll(\PDO::FETCH_ASSOC);

			$app->render('default.php',["data"=>$pessoas],200); 

		}
	}
}

?>