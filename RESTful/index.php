<?php
// Autoload
$loader = require 'vendor/autoload.php';
 
// Instanciando objeto
$app = new \Slim\Slim(array(
    'templates.path' => 'templates'
));

// Cadastra pessoa
$app->post('/pessoas/', function() use ($app){
	(new \controllers\Pessoa($app))->cadastrar();
});
 
// Pega pessoa pelo id
$app->get('/pessoas/:id', function($id) use ($app){
	(new \controllers\Pessoa($app))->pegar($id);
});
 
// Edita pessoa
$app->put('/pessoas/:id', function($id) use ($app){
	(new \controllers\Pessoa($app))->editar($id);
});
 
// Remove pessoa
$app->delete('/pessoas/:id', function($id) use ($app){
	(new \controllers\Pessoa($app))->excluir($id);
});

// Listagem pessoas
$app->get('/pessoas/', function() use ($app){
	(new \controllers\Pessoa($app))->listar();
});
 
// Rodando aplicação
$app->run();
?>