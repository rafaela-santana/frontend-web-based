
app.controller('HomeCtrl', function($scope, $http, $location, $filter, $dialog, Pessoas) {

    $scope.buscar = "";

    //Atualiza listagem de pessoas
    $scope.atualiza_listagem = function() {
        Pessoas.query({}, function(response) {
            $scope.pessoas = response;
            $scope.pessoas = $filter('orderBy')($scope.pessoas, 'nome');
            $scope.pessoas_completo = $scope.pessoas;
        });
    };

    $scope.atualiza_listagem();

    //Busca entre as pessoas cadastradas
    $scope.buscar_pessoa = function(id) {

        $scope.pessoas = $filter('filter')($scope.pessoas_completo, {'identificacao':$scope.buscar});

        angular.extend($scope.pessoas, $filter('filter')($scope.pessoas_completo, {'nome':$scope.buscar}));
    }

    //Redireciona para o editar cadastro
    $scope.editar_pessoa = function(id) {
        $location.path('/editar/'+id);
    };

    //Redireciona para o cadastro de pessoa
    $scope.cadastrar_pessoa = function() {
        $location.path('/cadastrar');
    };

    $scope.remover_pessoa = function(id) {

        console.log("id",id);
        $scope.removido = id;
        $scope.showModal = true;
        
    };

    //Remove a pessoa se a opção Sim for selecionada no modal
    $scope.remover = function() {

        Pessoas.get({ id:$scope.removido+".json" }, function(response) {

            $scope.pessoa = response;

            Pessoas.remove({id:$scope.removido}, JSON.stringify($scope.pessoa),function(response) {
                $scope.atualiza_listagem();
                $scope.showModal = false;
            },function(response) {
                console.log("Ocorreu um erro ao tentar remover o cadastro.", response);
                $scope.atualiza_listagem();
                $scope.showModal = false;
            });

        });
        
    };
    
    //Fecha o modal
    $scope.close = function(){
        $scope.showModal = false;
    };

})
.controller('EditarCtrl', function($scope, $http , $routeParams, $location, $filter, Pessoas) {
    
    //Pega as informacoes das pessoas
    Pessoas.query({}, function(response) {
        $scope.pessoas = response;
        $scope.pessoas = $filter('orderBy')($scope.pessoas, 'nome');
    });

    //Pega a informações da pessoa selecionada
    Pessoas.get({ id:$routeParams.id }, function(response) {
        $scope.pessoa = response;
    });

    $scope.hoje = new Date();

    //Salva as alterações realizadas no cadastro
    $scope.salvar = function() {
        var mensagem = "";
        var error = 0;

        $scope.pessoa.nome = $scope.pessoa.nome.trim();
        $scope.pessoa.sexo = $scope.pessoa.sexo.trim();
        $scope.pessoa.identificacao = $scope.pessoa.identificacao.trim();
        $scope.pessoa.endereco = $scope.pessoa.endereco.trim();

       //Verificações nome
        $scope.errors = new Array();

        if ($scope.pessoa.nome.trim() == "") {
            error++;
            $scope.errors.nome = "O campo nome não pode ficar em branco.";
        } 

        //Verificação dataNascimento
        var data_nasc = moment($scope.pessoa.dataNascimento);
        var data_hoje = moment();
        if ($scope.pessoa.dataNascimento == "") {
            error++;
            $scope.errors.dataNascimento = "O campo data de nascimento não pode ficar em branco.";
        }
        if (!data_nasc.isValid()) {
            error++;
            $scope.errors.dataNascimento = "O campo data de nascimento deve conter uma data válida.";
        }
        if (data_nasc > data_hoje) {
            error++;
            $scope.errors.dataNascimento = "O campo data de nascimento não pode ser maior que a data corrente.";
        } 

        //Verificação sexo
        if ($scope.pessoa.sexo == "") {
            error++;
            $scope.errors.sexo = "O campo sexo não pode ficar em branco.";
        } 

        //Verificações idenfiticação
        if ($scope.verifica_identificacao()) {
            error++;
            $scope.errors.identificacao = "Já existe uma pessoa com essa identificação.";
        }
        if ($scope.pessoa.identificacao == "") {
            error++;
            $scope.errors.identificacao = "O campo identificação não pode ficar em branco.";
        }

        if (error == 0) {

            Pessoas.update({id:$routeParams.id}, JSON.stringify($scope.pessoa),function(response) {
                $location.path('/');
            },function(response) {
                console.log("Ocorreu um erro ao atualizar!");
                $location.path('/');
            });
            
        }

    };

    $scope.verifica_identificacao = function(){
        var elementos = $filter('filter')($scope.pessoas, {'identificacao':$scope.pessoa.identificacao}) ;
        var resultado = false;
        
        elementos.forEach( function(elemento, index) {
            if (elemento.id != $scope.pessoa.id) {
                resultado = true;
            }
        });

        return resultado;
    };
      
    $scope.voltar = function(){
        $location.path('/');
    };
    
})
.controller('CadastrarCtrl', function($scope, $http, $location, $filter, Pessoas) {

    //Pega as informacoes das pessoas
    Pessoas.query({}, function(response) {
        $scope.pessoas = response;
        $scope.pessoas = $filter('orderBy')($scope.pessoas, 'nome');
    });
    
    $scope.pessoa = {
        dataNascimento: "",
        endereco: "",
        identificacao: "",
        nome: "",
        sexo: ""
    };

    $scope.hoje = new Date();

    $scope.salvar = function() {
        var mensagem = "";
        var error = 0;

        $scope.pessoa.nome = $scope.pessoa.nome.trim();
        $scope.pessoa.dataNascimento = $scope.pessoa.dataNascimento.trim();
        $scope.pessoa.sexo = $scope.pessoa.sexo.trim();
        $scope.pessoa.identificacao = $scope.pessoa.identificacao.trim();
        $scope.pessoa.endereco = $scope.pessoa.endereco.trim();

        //Verificações nome
        $scope.errors = new Array();

        if ($scope.pessoa.nome == "") {
            error++;
            $scope.errors.nome = "O campo nome não pode ficar em branco.";
        } 

        //Verificação dataNascimento
        var data_nasc = moment($scope.pessoa.dataNascimento);
        var data_hoje = moment();    
        if ($scope.pessoa.dataNascimento == "") {
            error++;
            $scope.errors.dataNascimento = "O campo data de nascimento não pode ficar em branco.";
        }
        if (!data_nasc.isValid()) {
            error++;
            $scope.errors.dataNascimento = "O campo data de nascimento deve conter uma data válida.";
        }
        if (data_nasc > data_hoje) {
            error++;
            $scope.errors.dataNascimento = "O campo data de nascimento não pode ser maior que a data corrente.";
        }

        //Verificação sexo
        if ($scope.pessoa.sexo == "") {
            error++;
            $scope.errors.sexo = "O campo sexo não pode ficar em branco.";
        } 

        //Verificações idenfiticação        
        if ($scope.verifica_identificacao()) {
            error++;
            $scope.errors.identificacao = "Já existe uma pessoa com essa identificação.";
        }
        if ($scope.pessoa.identificacao == "") {
            error++;
            $scope.errors.identificacao = "O campo identificação não pode ficar em branco.";
        }

        if (error == 0) {

            Pessoas.create({}, JSON.stringify($scope.pessoa), function(response) {
                $location.path('/');
            },function(response) {
                console.log("Ocorreu um erro ao cadastrar!");
                $location.path('/');
            });
            
        }

    };

    $scope.verifica_identificacao = function(){
        var elementos = $filter('filter')($scope.pessoas, {'identificacao':$scope.pessoa.identificacao}) ;
        var resultado = false;
        
        elementos.forEach( function(elemento, index) {
            if (elemento.id != $scope.pessoa.id) {
                resultado = true;
            }
        });

        return resultado;
    };

    $scope.voltar = function(){
        $location.path('/');
    };
    
})
.controller('RelatoriosCtrl', function($scope, $http, Pessoas) {

    $scope.faixa_idade1 = 0;
    $scope.faixa_idade2 = 0;
    $scope.faixa_idade3 = 0;
    $scope.faixa_idade4 = 0;
    $scope.faixa_idade5 = 0;
    $scope.faixa_sexo1 = 0;
    $scope.faixa_sexo2 = 0;

    //Grafico da faixa de idades das pessoas, faixas: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"]
    $scope.graficoFaixaEtaria = {
        chart: {
            caption: "Pessoas por faixa etária",
            theme: "fint"
        },
        data: []
    };

    //Grafico contendo quantidade de pessoas que tenham sexo masculino e feminino
    $scope.graficoPessoaPorSexo = {
        chart: {
            caption: "Pessoas por sexo",
            startingangle: "0",
            showlabels: "0",
            showlegend: "1",
            enablemultislicing: "0",
            slicingdistance: "1",
            showpercentvalues: "1",
            showpercentintooltip: "0",
            theme: "fint"
        },
        data: []
    };

    //Pega as informacoes das pessoas
    Pessoas.query({}, function(response) {
        var pessoas = response;
   
        pessoas.forEach( function(pessoa, index) {
            var data_nascimento = moment(pessoa.dataNascimento);
            var idade = moment().diff(data_nascimento, 'years', true);

            if (idade < 10) {
                $scope.faixa_idade1 += 1;
            } else if (idade < 20) {
                $scope.faixa_idade2 += 1;
            } else if (idade < 30) {
                $scope.faixa_idade3 += 1;
            } else if (idade < 40) {
                $scope.faixa_idade4 += 1;
            } else {
                $scope.faixa_idade5 += 1;
            }

            if (pessoa.sexo == 1) {
                $scope.faixa_sexo1 += 1;
            } else {
                $scope.faixa_sexo2 += 1;
            }

        });

        $scope.graficoFaixaEtaria.data.push({
            'label': "0 a 9",
            'value': $scope.faixa_idade1.toString()
        });
        $scope.graficoFaixaEtaria.data.push({
            'label': "10 a 19",
            'value': $scope.faixa_idade2.toString()
        });
        $scope.graficoFaixaEtaria.data.push({
            'label': "20 a 29",
            'value': $scope.faixa_idade3.toString()
        });
        $scope.graficoFaixaEtaria.data.push({
            'label': "30 a 39",
            'value': $scope.faixa_idade4.toString()
        });
        $scope.graficoFaixaEtaria.data.push({
            'label': "Maior que 40",
            'value': $scope.faixa_idade5.toString()
        });

        $scope.graficoPessoaPorSexo.data.push({
            'label': "Feminino",
            'value': $scope.faixa_sexo2.toString()
        });
        $scope.graficoPessoaPorSexo.data.push({
            'label': "Masculino",
            'value': $scope.faixa_sexo1.toString()
        });

    }, function (resultado) {
        console.log("Ocorreu um erro ao buscar as informações.");
    });

})
