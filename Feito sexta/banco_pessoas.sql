-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 14-Out-2017 às 02:23
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pessoas_bd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `identificacao` varchar(50) NOT NULL,
  `dataNascimento` date NOT NULL,
  `sexo` tinyint(1) NOT NULL,
  `endereco` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoas`
--

INSERT INTO `pessoas` (`id`, `nome`, `identificacao`, `dataNascimento`, `sexo`, `endereco`, `created_at`, `updated_at`) VALUES
(1, 'Rafaela', '99886999', '1993-02-09', 2, 'Rua Pedro', '2017-10-13 22:55:50', '2017-10-13 22:55:50'),
(2, 'Rafael', '99886970', '1993-02-09', 1, 'Rua Pedro', '2017-10-13 22:56:05', '2017-10-14 00:12:23'),
(3, 'Maria Clara', '449393', '2005-03-21', 2, '', '2017-10-14 00:13:26', '2017-10-14 00:13:26'),
(4, 'Amanda Santana', '110202', '2002-09-25', 2, 'Av. Rondon Pacheco, nÂº 60', '2017-10-14 00:19:33', '2017-10-14 00:19:33'),
(5, 'Tarcisio Oliveira', '334411', '1970-11-03', 1, '', '2017-10-14 00:20:36', '2017-10-14 00:20:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
