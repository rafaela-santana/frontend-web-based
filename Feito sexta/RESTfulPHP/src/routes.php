<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

// get all pessoas
$app->get('/pessoas', function ($request, $response, $args) {

    $sth = $this->db->prepare("
    	SELECT * 
    	FROM pessoas 
    	ORDER BY nome
    ");
    $sth->execute();

    $pessoas = $sth->fetchAll();

    return $this->response->withJson($pessoas);
});

// get pessoa pelo id 
$app->get('/pessoa/[{id}]', function ($request, $response, $args) {

    $sth = $this->db->prepare("
    	SELECT * 
    	FROM pessoas 
    	WHERE id = :id
    ");

    $sth->bindParam("id", $args['id']);

    $sth->execute();

    $pessoa = $sth->fetchObject();

    return $this->response->withJson($pessoa);
});

// cria uma nova pessoa
$app->post('/pessoa', function ($request, $response) {

    $input = $request->getParsedBody();

    $sth = $this->db->prepare("
    	INSERT INTO pessoas (nome,identificacao,dataNascimento,sexo,endereco) 
    	VALUES (:nome,:identificacao,:dataNascimento,:sexo,:endereco) 
    ");

    $sth->bindParam("nome", $input['nome']);
    $sth->bindParam("identificacao", $input['identificacao']);
    $sth->bindParam("dataNascimento", $input['dataNascimento']);
    $sth->bindParam("sexo", $input['sexo']);
    $sth->bindParam("endereco", $input['endereco']);

    $sth->execute();

    $input['id'] = $this->db->lastInsertId();

    return $this->response->withJson($input);
});

// DELETE uma pessoa pelo id informado
$app->delete('/pessoa/[{id}]', function ($request, $response, $args) {

    $sth = $this->db->prepare("
    	DELETE FROM pessoas 
    	WHERE id = :id
    ");

    $sth->bindParam("id", $args['id']);

    $sth->execute();

    $todos = $sth->fetchAll();

    return $this->response->withJson($todos);
});

// Edita uma pessoa pelo id informado
$app->put('/pessoa/[{id}]', function ($request, $response, $args) {

    $input = $request->getParsedBody();

    $sth = $this->db->prepare("
    	UPDATE pessoas 
    	SET 
    		nome = :nome,
    		identificacao = :identificacao,
    		dataNascimento = :dataNascimento,
    		sexo = :sexo,
    		endereco = :endereco
    	WHERE 
    		id = :id
    ");

    $sth->bindParam("id", $args['id']);
    $sth->bindParam("nome", $input['nome']);
    $sth->bindParam("identificacao", $input['identificacao']);
    $sth->bindParam("dataNascimento", $input['dataNascimento']);
    $sth->bindParam("sexo", $input['sexo']);
    $sth->bindParam("endereco", $input['endereco']);

    $sth->execute();

    $input['id'] = $args['id'];

    return $this->response->withJson($input);
});